const express = require('express');
const app = express();

app.get('/test', (req, res) => {
    return res.send(
        `<html><body><h2>I'm working...</h2></body></html>`
    );
});

app.get('/me', (req, res) => {
    return res.send(
        `<html><body><h2>Hello there!!!</h2></body></html>`
    );
});

app.get('/hi', (req, res) => {
    return res.send(
        `<html><body><h2>Hey there!!!</h2></body></html>`
    );
});

app.get('/a', (req, res) => {
    return res.send(
        `<html><body><h2>AAA</h2></body></html>`
    );
});

const port = 3000;
const server = app.listen(port, () => console.log(`Listening on port ${port}...`));